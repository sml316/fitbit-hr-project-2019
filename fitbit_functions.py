import fitbit
import gather_keys_oauth2 as Oauth2
import pandas as pd
from datetime import datetime as dt
import datetime


def user_authorisation(client_id, client_secret):
    '''Authorisation through Oauth2 module.

    Client ID and Client Secret are string types.

    :param client_id: Client ID can be obtained by filling in an application form online. 6 digit code.
    :param client_secret: Long string of secret code also can be obtained the same way as client_id.
    :return: using the returned auth2_client variable allows straight forward authorisation
    '''

    try:
        server = Oauth2.OAuth2Server(client_id, client_secret)
        server.browser_authorize()
        access_token = str(server.fitbit.client.session.token["access_token"])
        refresh_token = str(server.fitbit.client.session.token["refresh_token"])
        auth2_client = fitbit.Fitbit(client_id, client_secret, oauth2=False, access_token=access_token,
                                     refresh_token=refresh_token)
        return auth2_client

    except:
        print("Authorisation failed, check client_id and client_secret, and check that they are string types")


def get_end_list(j, end_active_time_list, df):
    end_workout = dt.strptime(df.iloc[j]['time'], "%H:%M:%S").time()
    end_active_time_list.append(end_workout)
    return end_active_time_list


def get_resting_hr(list_of_dict):
    '''Resting HR and Date is present if the user has worn the tracker for long enough that day.

    :param list_of_dict: This is a list of dictionaries within ['activities/heart']
    :return date_list: list of dates where resting HR was logged
    :return r_hr_list: list of resting HR for dates

    '''

    date_list = []
    r_hr_list = []
    for dict1 in list_of_dict:
        if 'restingHeartRate' in dict1['value']:
            date_list.append(dict1['dateTime'])
            r_hr_list.append(dict1['value']['restingHeartRate'])

    return date_list, r_hr_list


def calculated_calories(bmr, duration, met=5):
    '''Manually estimates calorie spent for a MET 5 exercise based on extracted BMR'''
    return (int(bmr)/1440) * int(duration) * met


def get_detail_hr(get_detail):
    '''Gets logged time stamps and HR values for one day'''
    time_list = []
    val_list = []
    for j in get_detail['activities-heart-intraday']['dataset']:
        val_list.append(j['value'])
        time_list.append(j['time'])
    return time_list, val_list


def add_rolling_mean(df):
    '''Rolling average HR of 20minute window is calculated and added onto data frame.

    :param df: original data frame with timestamps and HR values
    :return: new data frame with a new column of rolling mean added.
    '''

    rolling_df = df['value'].rolling(240).mean()
    df = df.assign(rolling_mean=rolling_df)
    return df


def get_active_hr(df):
    '''Active period detected if rolling average HR is equal to, or greater than 120bpm.

    :param df: data frame that has timestamp, HR value and rolling_mean columns
    :return timestamps: a list of timestamps during exercise period
    :return active_hr: a list of HR values during exercise period
    '''

    timestamps = []
    active_hr = []

    for j in range(0, len(df['value']) - 1):
        # value 120 should be the mean value of HR during workout
        if df.loc[j]['rolling_mean'] >= 120:
            timestamps.append(df.loc[j]['time'])
            active_hr.append((df.loc[j]['value']))
        else:
            pass

    return timestamps, active_hr


def merge_workout(start_active_time_list, end_active_time_list):
    '''Workouts that are within 60minutes to each other is considered as one

    :param start_active_time_list: list of exercise start times
    :param end_active_time_list: list of exercise end times
    :return: lists after joining workouts
    '''

    counter = 0

    # if final exercise time went over midnight, length of two lists do not match up.
    if len(start_active_time_list) != len(end_active_time_list):
        del start_active_time_list[-1]
        print('removed last entry')

    # if there was 4 exercise time detected, check three times.
    limit = len(start_active_time_list) - 1

    for i in range(limit):
        # convert time object into datetime object to subtract values
        next_start_dt = dt.combine(datetime.date.today(), start_active_time_list[counter+1])
        earlier_end_dt = dt.combine(datetime.date.today(), end_active_time_list[counter])

        # if two workouts are within 60 minutes, shift values and delete old values
        if next_start_dt - earlier_end_dt < datetime.timedelta(minutes=60):
            end_active_time_list[counter] = end_active_time_list[counter+1]
            del end_active_time_list[counter+1]
            del start_active_time_list[counter+1]

        else:
            counter += 1

    return start_active_time_list, end_active_time_list


def get_index(i, start_active_time_list, end_active_time_list, df):
    '''Gets index values of data frame, where they match start/end workout times'''

    series = pd.Index(df['time'])
    start_index_value = series.get_loc(str(start_active_time_list[i]))
    end_index_value = series.get_loc(str(end_active_time_list[i]))

    print(start_index_value, end_index_value)
    return start_index_value, end_index_value


def get_average_hr(i, start_active_time_list, end_active_time_list, df):
    '''Returns the average HR during a workout period.

    :param i: index to get values from start/end_active_time_list
    :param start_active_time_list: a list of exercise start times
    :param end_active_time_list: a list of exercise end times
    :param df: Dataframe of one day, with three colums = [timestamps, HR, rollingHR]
    :return: returns the average HR value during an exercise session.
    counter: counts the number of Dataframe rows the loop iterates through, to divide the sum_hr
    '''

    sum_hr = 0
    counter = 0

    # extracts the index value using get_index()
    start_index_value, end_index_value = get_index(i, start_active_time_list, end_active_time_list, df)

    while start_index_value < end_index_value:
        sum_hr += df.iloc[start_index_value]['value']
        counter += 1
        start_index_value += 1

    average_hr = sum_hr / counter

    return average_hr


def get_peak_counter(i, ave_hr_active_list, start_active_time_list, end_active_time_list, df):
    '''Returns number of HR peaks that correspond to working sets.
    It finds the index where the exercises starts/ends to iterate through HR between those times.
    HR must have dropped below the average HR (during that workout) to be able to detect the next HR peak.
    '''

    significance_flag = False
    peak_counter = 0

    # Get index of start/end workout times
    start_index_value, end_index_value = get_index(i, start_active_time_list, end_active_time_list, df)

    while start_index_value < end_index_value:
        if df.iloc[start_index_value]['value'] <= ave_hr_active_list[i]:
            significance_flag = False
            start_index_value += 1

        # while the flag is still False, if HR increases significantly, a HR peak is detected
        elif df.iloc[start_index_value]['value'] > 138.6 and significance_flag == False:
            significance_flag = True
            peak_counter += 1
            start_index_value += 1

        else:
            start_index_value += 1

    return peak_counter


