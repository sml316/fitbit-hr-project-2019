# Fitbit HR Project 2019

This software uses Fitbit API to perform data analysis on HR data to distinguish a weight lifting session vs. cardio session. Key difference used: mean HR during exercise and the number of HR peaks.