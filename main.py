import datetime
import pandas as pd
import fitbit_functions as fitfn
import time
from datetime import datetime as dt
import os
import os.path

# Input other user names if you have access to their data.
# Further information found in README.txt
username = input("Input your username, without spaces(use: MinLee)")

# Each app has a unique id and secret upon requesting to fitbit
CLIENT_ID = 'XXXXXX'
CLIENT_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXX'
auth2_client = fitfn.user_authorisation(CLIENT_ID, CLIENT_SECRET)

yesterday = str((datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d"))

# Gets all heart rate activities in the recent 1 year that is related to activity.
get_activities_HR = auth2_client.time_series('activities/heart', base_date=yesterday, period='1y')

# assigned it to a variable (type: list of dicts) to reduce complexity.
list_of_HR = get_activities_HR['activities-heart']
date_list, rHR_list = fitfn.get_resting_hr(list_of_HR)
rHR_df = pd.DataFrame({'dateTime': date_list, 'resting HR': rHR_list})

# create a main path folder with a daughter folder inside called HRdaily
path_main = str("output_data_" + username)
if os.path.exists(path_main+"/HRdaily"):
    pass

else:
    os.makedirs(path_main+"/HRdaily")

os.chdir(path_main+"/HRdaily")

'''for each date in date_list, get all timestamps and HR values. Then save it as a csv file'''
for i in range(1, len(date_list)+1):

    # start from the most recent date
    date = date_list[-i]

    # avoid repeating code for files that already exists.
    if os.path.exists('HR' + date + '.csv'):
        pass

    else:
        # call intraday_time_series() to get intraday data
        get_detail = auth2_client.intraday_time_series('activities/heart', base_date=date, detail_level='1sec')
        # get_detail() returns a list of times, and a list of HR values.
        time_list, val_list = fitfn.get_detail_hr(get_detail)

        # pause and avoid giving too much request to Fitbit API server
        time.sleep(25)

        # at the end of each loop, lists are made into pandas data frame and saved at "HRdaily"
        detail_HR_df = pd.DataFrame({'time': time_list, 'value': val_list})
        detail_HR_df.to_csv("HR" + date + '.csv', columns=['time', 'value'], header=True, index=False)


# still in directory "HRdaily"
df_active = pd.DataFrame()
df = pd.DataFrame()
collection_df = pd.DataFrame()
active_HR = []
# test_list = ["2019-05-18", "2019-04-14"]
for i in reversed(date_list):
    date = i

    # reset lists for every date
    ave_HR_active_list = []
    peak_counter_list = []
    dates = []
    start_active_time_list = []
    end_active_time_list = []
    daily_collection_df = pd.DataFrame()
    workout_duration_list = []

    try:
        df = pd.read_csv('HR' + i + ".csv")
        # rolling mean column added to df
        df = fitfn.add_rolling_mean(df)

        # if the user did not exercise this day, df_active will be empty
        timestamps, active_HR = fitfn.get_active_hr(df)
        df_active = pd.DataFrame(list(zip(timestamps, active_HR)), columns=['active_time', 'active_value'])

    except:
        # no intraday data considered as no exercise for that day.
        df_active = pd.DataFrame()

    if df_active.empty:
        # if the user has not been active that day, None is appended to the lists
        start_active_time_list.append(None)
        end_active_time_list.append(None)
        ave_HR_active_list.append(None)
        peak_counter_list.append(None)
        workout_duration_list.append(None)

    else:
        # user has been active that day
        # reset startWorkout and counter for every date.
        startWorkout = 0
        # counter = 0

        # j cannot be 0 because then [j-1] will get the last element of the list.
        for j in range(1, len(df['value'])):

            # time where the 20min rolling HR reaches 110 is the start of activity
            if (df.iloc[j]['rolling_mean'] >= 110) and (df.iloc[j - 1]['rolling_mean'] < 110):
                startWorkout = dt.strptime(df.iloc[j]['time'], "%H:%M:%S").time()
                # adds the timestamp to the start_active_time_list.
                start_active_time_list.append(startWorkout)

            # time where the 20min rolling HR drops below 110 is the end of activity
            elif (df.iloc[j]['rolling_mean'] < 110) and (df.iloc[j - 1]['rolling_mean'] >= 110):

                # do not add ending time if there was no starting time
                if startWorkout == 0:
                    pass

                else:
                    end_active_time_list = fitfn.get_end_list(j, end_active_time_list, df)

        # merge_workout() merges consecutive workouts if they are within 60 minutes
        start_active_time_list, end_active_time_list = fitfn.merge_workout(start_active_time_list, end_active_time_list)


        # if there was one object in start_active_time list which was then removed due to end time being over midnight,
        # that leaves both lists empty. In this case, append none to lists.

        for s in range(len(start_active_time_list)):
            dates.append(i)

            # datetime.time objects in active_time_lists converted into datetime to be subtracted
            WorkoutDuration = dt.combine(datetime.date.today(), end_active_time_list[s]) - dt.combine(datetime.date.today(), start_active_time_list[s])
            WorkoutDuration = (datetime.datetime.min + WorkoutDuration).time()
            workout_duration_list.append(WorkoutDuration)

            # Iterates through each start time, extracts index of df, sums up HR over that time period
            averageHR = fitfn.get_average_hr(s, start_active_time_list, end_active_time_list, df)
            ave_HR_active_list.append(averageHR)
            peak_counter = fitfn.get_peak_counter(s, ave_HR_active_list, start_active_time_list,
                                                  end_active_time_list, df)
            peak_counter_list.append(peak_counter)

        daily_collection_df = daily_collection_df.assign(date=dates, start=start_active_time_list,
                                                         end=end_active_time_list, duration=workout_duration_list,
                                                         mean_HR=ave_HR_active_list, sets=peak_counter_list)

        print(daily_collection_df)
        collection_df = pd.concat([collection_df, daily_collection_df])

    print(collection_df)

# go up one level to the main directory to output_data[username]
os.chdir("..")
collection_df.to_csv(username + 'ActivitySummary.csv', columns=["date", "start", "end", "duration", "mean_HR", "sets"],
                     header=True, index=False)

# Now process the collection_df to collect weight lifting data only
get_calories_BMR = auth2_client.time_series('activities/caloriesBMR', base_date=yesterday, period="1y")

weights_df = pd.DataFrame()

calorie_list = []
dateTime_list = []
duration_ms = []
startTime_list = []
sets_list = []

# iterate through each row of collection_df
for i in range(len(collection_df['date'])):

        # sessions with 3 or more sets are weight lifting sessions hence append values to lists
        if collection_df.iloc[i]['sets'] >= 3:
            startTime_list.append(collection_df.iloc[i]['start'])

            bmr_extract = get_calories_BMR['activities-caloriesBMR'][i]['value']

            # get duration in datetime.time() object
            duration = collection_df.iloc[i]['duration']
            # convert duration into minutes to calculate calories
            activity_duration = int(duration.minute + duration.hour * 60 + int(duration.second/60))
            # calculate calories burned and append to list
            calorie_list.append(fitfn.calculated_calories(bmr_extract, activity_duration))

            # convert duration into milliseconds since it is a required parameter for log_activity().
            duration_ms.append((duration.hour * 3600 + duration.minute * 60 + duration.second)*1000)

            sets_list.append(collection_df.iloc[i]['sets'])

            # convert string into datetime.date() object
            dateTime_list.append(collection_df.iloc[i]['date'])

        else:
            pass

# assign lists as columns in weights_df to save it as a csv file.
weights_df = weights_df.assign(date=dateTime_list, start=startTime_list, duration_ms=duration_ms, calories=calorie_list,
                               sets=sets_list)
weights_df.to_csv(username + "Weights.csv", columns=["date", "start", "duration_ms", "calories", "sets"], header=True,
                  index=False)
print(weights_df)


# log the most recent 7 weight lifting sessions.
# to log all data in weights_df, unhash line 209 and hash line 210.
# for i in range(len(weights_df[df])):
for i in range(7):
    try:
        data = {"activityName": "Weights", "manualCalories": int(calorie_list[i]), "date": dateTime_list[i],
                "durationMillis": int(duration_ms[i]), "startTime": startTime_list[i]}

        get_activities = auth2_client.log_activity(data)
        print(data)
        print(i)

    except IndexError:
        print('No data to log for this day')

